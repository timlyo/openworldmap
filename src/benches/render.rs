extern crate criterion;

use criterion::{Criterion, criterion_group, criterion_main};
use criterion::BatchSize;

use open_world_map::render::world_to_pixels;
use open_world_map::world::random_world;

fn benchmark_world_to_pixels(c: &mut Criterion) {
    c.bench_function_over_inputs(
        "world_to_pixels 14 points",
        |b, size| b.iter_batched(
            || random_world(**size),
            |world| world_to_pixels(&world),
            BatchSize::SmallInput,
        ),
        &[10.0, 20.0, 30.0, 40.0, 50.0, 100.0, 150.0, 200.0],
    );
}

criterion_group!(benches, benchmark_world_to_pixels);
criterion_main!(benches);
