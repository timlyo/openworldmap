extern crate png;

use std::fs::File;
use std::io;
use std::io::BufWriter;
use std::ops;
use std::path::Path;

use itertools::iproduct;
use png::HasParameters;

use crate::World;

#[derive(Clone)]
struct Pixel {
    r: u8,
    g: u8,
    b: u8,
    a: u8,
}

impl Pixel {
    pub fn new(r: u8, g: u8, b: u8, a: u8) -> Pixel {
        Pixel { r, g, b, a }
    }

    pub fn to_array(&self) -> Vec<u8> {
        vec![self.r, self.g, self.b, self.a]
    }

    fn white() -> Pixel {
        Pixel {
            r: 255,
            g: 255,
            b: 255,
            a: 255,
        }
    }
}

impl ops::Add<u8> for Pixel {
    type Output = Self;

    fn add(self, rhs: u8) -> Self::Output {
        Pixel {
            r: self.r.checked_add(rhs).unwrap_or(255),
            g: self.g.checked_add(rhs).unwrap_or(255),
            b: self.b.checked_add(rhs).unwrap_or(255),
            a: self.a,
        }
    }
}

pub fn world_to_pixels(world: &World) -> Vec<u8> {
    let coordinates = iproduct!((0..world.width as u32),(0..world.height as u32));

    coordinates
        // TODO use multiple threads
        .map(|(x, y)| pos_to_pixel(x as f64, y as f64, world))
        .map(|pixel| pixel.unwrap_or(Pixel::white()))
        .flat_map(|pixel| pixel.to_array())
        .collect()
}

fn pos_to_pixel(x: f64, y: f64, world: &World) -> Option<Pixel> {
    let elevation = world.elevation.value_at_x_y(x, y)?;

    Some(if elevation < world.sealevel - 5.0 {
        Pixel::new(0u8, 105u8, 148u8, 255u8)
    } else if elevation < world.sealevel {
        Pixel::new(50u8, 125u8, 178u8, 255u8)
    } else {
        let gradient = (elevation - world.sealevel) as u8;
        if elevation > 80.0 {
            Pixel::new(255u8, 255u8, 255u8, 255u8) + gradient
        } else if elevation > 60.0 {
            Pixel::new(110u8, 112u8, 117u8, 255u8) + gradient
        } else {
            Pixel::new(96u8, 128, 56, 255) + gradient
        }
    })
}

pub fn render_world<P: AsRef<Path>>(world: &World, file: P) -> Result<(), io::Error> {
    let data = world_to_pixels(world);
    dbg!(&data.len());


    let file = File::create(file)?;
    let ref mut writer = BufWriter::new(file);

    let mut encoder = png::Encoder::new(writer, world.width as u32, world.height as u32);
    encoder.set(png::ColorType::RGBA).set(png::BitDepth::Eight);
    let mut writer = encoder.write_header()?;

    writer.write_image_data(&data)?;
    Ok(())
}

#[cfg(test)]
pub mod tests {
    use crate::world::random_world;

    use super::*;

    #[test]
    #[ignore]
    fn test_render_world() {
        let world = random_world(500.0);

        render_world(&world, "test.png").expect("Couldn't render world");
    }
}

