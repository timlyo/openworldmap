pub use self::datapoint::DataPoint;
pub use self::layer::NumericLayer;

mod layer;
mod datapoint;

#[derive(Debug, Clone)]
pub struct World {
    pub name: String,
    pub height: f64,
    pub width: f64,
    pub sealevel: f64,
    // TODO save to db
    pub elevation: NumericLayer,
    pub rainfall: NumericLayer,
}


impl World {
    pub fn new(name: String, width: f64, height: f64) -> World {
        World {
            name,
            width,
            height,
            sealevel: 20.0,
            elevation: NumericLayer::new(),
            rainfall: NumericLayer::new(),
        }
    }
}

pub fn random_world(size: f64) -> World {
    use rand::{thread_rng, Rng};

    let mut world = World::new("test".to_owned(), size, size);

    world.elevation.insert(vec![DataPoint::new(0.0, 0.0, 0.0),
                                DataPoint::new(size, 0.0, 0.0),
                                DataPoint::new(0.0, size, 0.0),
                                DataPoint::new(size, size, 0.0)]);

    let mut rng = thread_rng();
    let points = (0..10).map(|_| {
        let x = rng.gen_range(0.0, world.width);
        let y = rng.gen_range(0.0, world.height);
        let h = rng.gen_range(0.0, 100.0);

        DataPoint::new(x, y, h)
    }).collect();

    world.elevation.insert(points);

    world
}
