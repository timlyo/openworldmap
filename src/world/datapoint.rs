use cgmath::Point2;
use rusqlite::Row;
use spade::HasPosition;

#[derive(Debug, Clone)]
pub struct DataPoint {
    pub point: Point2<f64>,
    pub data: f64,
}

impl DataPoint {
    pub fn new(x: f64, y: f64, data: f64) -> DataPoint {
        DataPoint {
            point: Point2::new(x, y),
            data,
        }
    }

    pub fn from_row(row: &Row) -> Result<DataPoint, rusqlite::Error> {
        // TODO validation
        Ok(DataPoint::new(row.get(0), row.get(1), row.get(2)))
    }
}

impl HasPosition for DataPoint {
    type Point = Point2<f64>;

    fn position(&self) -> Self::Point {
        self.point
    }
}
