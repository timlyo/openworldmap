use std::fmt;
use std::ops::Deref;
use std::ops::DerefMut;

use cgmath::Point2;
use spade::delaunay::DelaunayTriangulation;
use spade::kernels::FloatKernel;
use spade::rtree::RTree;

use crate::world::datapoint::DataPoint;

#[derive(Clone)]
pub struct NumericLayer {
    pub points: RTree<DataPoint>,
    pub triangulation: DelaunayTriangulation<DataPoint, FloatKernel>,
}

impl fmt::Debug for NumericLayer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "points: {:?}", self.points)
    }
}

impl NumericLayer {
    pub fn new() -> NumericLayer {
        NumericLayer {
            points: RTree::new(),
            triangulation: DelaunayTriangulation::new(),
        }
    }

    pub fn value_at_x_y(&self, x: f64, y: f64) -> Option<f64> {
        self.value_at(&Point2::new(x, y))
    }

    pub fn value_at(&self, pos: &Point2<f64>) -> Option<f64> {
        self.triangulation.barycentric_interpolation(pos, |x| x.data)
    }

    /// Insert points and update triangulation
    pub fn insert(&mut self, points: Vec<DataPoint>) {
        for point in points {
            self.points.insert(point.clone());
            self.triangulation.insert(point);
        }
    }
}

impl Deref for NumericLayer {
    type Target = RTree<DataPoint>;

    fn deref(&self) -> &Self::Target {
        &self.points
    }
}

impl DerefMut for NumericLayer {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.points
    }
}

#[cfg(test)]
mod tests {
    extern crate assert_approx_eq;

    use assert_approx_eq::assert_approx_eq;

    use super::*;

    #[test]
    fn value_at_between_4_equal_points_is_equal_to_those_points() {
        let mut layer = NumericLayer::new();

        // make flat plane at height 5.0
        layer.insert(vec![DataPoint::new(0.0, 0.0, 5.0),
                          DataPoint::new(10.0, 0.0, 5.0),
                          DataPoint::new(0.0, 10.0, 5.0),
                          DataPoint::new(10.0, 10.0, 5.0)]);

        // check 10 random points
        for _ in 0..10 {
            let x = rand::random();
            let y = rand::random();

            let value = layer.value_at(&Point2::new(x, y));

            assert_approx_eq!(value.unwrap(), 5.0);
        }
    }

    #[test]
    fn value_at_with_only_3_points() {
        let mut layer = NumericLayer::new();

        // make flat plane at height 5.0
        layer.insert(vec![DataPoint::new(0.0, 0.0, 5.0),
                          DataPoint::new(10.0, 0.0, 5.0),
                          DataPoint::new(0.0, 10.0, 5.0)]);

        let x = rand::random();
        let y = rand::random();

        let value = layer.value_at(&Point2::new(x, y));

        assert_approx_eq!(value.unwrap(), 5.0);
    }
}