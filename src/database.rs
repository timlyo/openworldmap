use rusqlite::{Connection, NO_PARAMS};

use crate::world::DataPoint;
use crate::world::NumericLayer;
use crate::world::World;

pub trait Database {
    //TODO replace return with generic error
    fn save_world(&self, world: &World) -> Result<(), rusqlite::Error>;
    fn load_world(&self) -> Result<World, rusqlite::Error>;
}


pub struct SqLite(Connection);

impl SqLite {
    pub fn get_connection(name: &str) -> Result<SqLite, rusqlite::Error> {
        let name = format!("{}.sqlite3", name);
        let connection = if cfg!(feature = "in-memory-db") {
            Connection::open_in_memory()?
        } else {
            Connection::open(name)?
        };

        Ok(SqLite(connection))
    }

    fn create_tables(&self, world: &World) -> Result<(), rusqlite::Error> {
        self.0.execute(r#"
            CREATE TABLE info (
                name TEXT NOT NULL,
                width REAL NOT NULL,
                height REAL NOT NULL
            )"#, NO_PARAMS)?;

        dbg!(world.width);
        self.0.prepare("
            INSERT INTO info
                (name, width, height)
            VALUES
                (?1, ?2, ?3)
        ")?.execute(&[&world.name, &world.width.to_string(), &world.height.to_string()])?;

        self.0.execute(r#"
            CREATE TABLE elevation(
                x REAL,
                y REAL,
                elevation REAL
            )"#, NO_PARAMS)?;

        Ok(())
    }

    fn get_info(&self) -> Result<(String, f64, f64), rusqlite::Error> {
        let mut statement = self.0.prepare("SELECT name, width, height from info")?;
        statement.query_row(NO_PARAMS, |row| {
            (row.get(0), row.get(1), row.get(2))
        })
    }

    fn get_elevations(&self) -> Result<Vec<DataPoint>, rusqlite::Error> {
        let mut statement = self.0.prepare("SELECT x, y, elevation from elevation")?;
        let elevations = statement
            .query_and_then(NO_PARAMS, |r| DataPoint::from_row(r))?
            .filter_map(Result::ok)
            .collect();

        Ok(elevations)
    }

    fn save_elevation(&self, elevations: &NumericLayer) -> Result<(), rusqlite::Error> {
        let mut statement = self.0.prepare(r#"
            INSERT INTO elevation (x, y, elevation)
            VALUES (?1, ?2, ?3)"#)?;

        for elevation in elevations.iter() {
            statement.execute(&[elevation.point.x, elevation.point.y, elevation.data])?;
        }
        Ok(())
    }
}

impl Database for SqLite {
    fn save_world(&self, world: &World) -> Result<(), rusqlite::Error> {
        self.create_tables(world)?;

        self.save_elevation(&world.elevation)?;

        Ok(())
    }

    fn load_world(&self) -> Result<World, rusqlite::Error> {
        let info = self.get_info()?;

        let mut world = World::new(info.0, info.1, info.2);

        world.elevation.insert(self.get_elevations()?);

        Ok(world)
    }
}
