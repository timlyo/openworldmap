use open_world_map;
use open_world_map::database::Database;
use open_world_map::database::SqLite;
use open_world_map::World;
use open_world_map::world::DataPoint;

fn create_world() -> World {
    World::new("test".to_owned(), 50.0, 50.0)
}

fn save_and_load_world(world: &World) -> World {
    let db = SqLite::get_connection(&world.name).expect("Couldn't get db connection");
    db.save_world(&world).expect("Couldn't save world");

    db.load_world()
        .expect("Couldn't load world")
}

#[test]
fn saving_and_loading_produces_the_same_info() {
    let mut world = create_world();

    world.elevation.insert(vec![DataPoint::new(10.0, 10.0, 10.0)]);

    let loaded_world = save_and_load_world(&world);

    assert_eq!(world.name, loaded_world.name);
    assert_eq!(world.width, loaded_world.width);
    assert_eq!(world.height, loaded_world.height);
}

#[test]
fn saving_and_loading_produces_the_same_elevation() {
    let mut world = create_world();

    assert_eq!(world.elevation.size(), 0);

    let elevations = vec![
        DataPoint::new(10.0, 10.0, 10.0),
        DataPoint::new(20.0, 20.0, 20.0),
        DataPoint::new(30.0, 30.0, 30.0)
    ];

    world.elevation.insert(elevations.clone());

    let loaded_world_elevations: Vec<DataPoint> = save_and_load_world(&world)
        .elevation.iter().cloned().collect();

    dbg!(&loaded_world_elevations);
    assert_eq!(elevations.len(), loaded_world_elevations.len());
}
